include vendor/kaukov/config/BoardConfigKernel.mk

ifeq ($(BOARD_USES_QCOM_HARDWARE),true)
include vendor/kaukov/config/BoardConfigQcom.mk
endif

include vendor/kaukov/config/BoardConfigSoong.mk
